package com.example.damsobiapp.ui.detail


import com.bagicode.foodmarketkotlin.model.response.checkout.CheckoutResponse
import com.example.damsobiapp.base.BasePresenter
import com.example.damsobiapp.base.BaseView


interface PaymentContract {
    interface  View: BaseView {
        fun onCheckoutSuccess(checkoutResponse: CheckoutResponse, view: android.view.View)
        fun onCheckoutFailed(message:String)

    }
    interface  Presenter : PaymentContract, BasePresenter {
        fun getCheckout(foodId:String, userId:String, quantity:String,total:String,view: android.view.View)

    }
}