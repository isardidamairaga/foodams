package com.example.damsobiapp.ui.profile

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.damsobiapp.ui.home.newtaste.HomeNewTasteFragment
import com.example.damsobiapp.ui.home.popular.HomePopularFragment
import com.example.damsobiapp.ui.home.recommended.HomeRecommendedFragment
import com.example.damsobiapp.ui.profile.account.ProfileAccountFragment
import com.example.damsobiapp.ui.profile.foodams.ProfileFoodamsFragment

class SectionPagerAdapter (fm:FragmentManager) : FragmentPagerAdapter(fm){

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0->"Account"
            1->"Foodams"
            else ->""
        }
    }
    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {
        var fragment : Fragment
        return when(position){
            0 ->{
                fragment = ProfileAccountFragment()
                return  fragment
            }
            1 ->{
                fragment = ProfileFoodamsFragment()
                return  fragment
            }
            else ->{
                fragment = ProfileAccountFragment()
                return  fragment
            }
        }
    }
}