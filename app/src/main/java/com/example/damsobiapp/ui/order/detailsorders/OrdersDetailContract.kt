package com.example.damsobiapp.ui.order.detailsorders

import com.example.damsobiapp.base.BasePresenter
import com.example.damsobiapp.base.BaseView

interface OrdersDetailContract {
    interface View : BaseView {
        fun onUpdateTransactionSuccess(message: String)
        fun onUpdateTransactionFailed(message: String)
    }

    interface Presenter : OrdersDetailContract, BasePresenter {
        fun getUpdateTransaction(id:String, status:String)
    }
}