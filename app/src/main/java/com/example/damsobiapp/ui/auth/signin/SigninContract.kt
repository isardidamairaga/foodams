package com.example.damsobiapp.ui.auth.signin

import com.example.damsobiapp.base.BasePresenter
import com.example.damsobiapp.base.BaseView
import com.example.damsobiapp.model.response.login.LoginResponse

interface SigninContract {
    interface  View: BaseView {
        fun onLoginSuccess(loginResponse: LoginResponse)
        fun onLoginFailed(message:String)

    }
    interface  Presenter : SigninContract, BasePresenter {
        fun submitLogin(email:String, password:String)

    }
}