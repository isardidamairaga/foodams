package com.example.damsobiapp.ui.home

import com.example.damsobiapp.base.BasePresenter
import com.example.damsobiapp.base.BaseView
import com.example.damsobiapp.model.response.home.HomeResponse

interface HomeContract {
    interface  View: BaseView {
        fun onHomeSuccess(homeResponse: HomeResponse)
        fun onHomeFailed(message:String)

    }
    interface  Presenter : HomeContract, BasePresenter {
        fun getHome()

    }
}