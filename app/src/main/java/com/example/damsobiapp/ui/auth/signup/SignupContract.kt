package com.example.damsobiapp.ui.auth.signup

import android.net.Uri
import com.example.damsobiapp.base.BasePresenter
import com.example.damsobiapp.base.BaseView
import com.example.damsobiapp.model.request.RegisterRequest
import com.example.damsobiapp.model.response.login.LoginResponse
import java.security.cert.CertPath

interface SignupContract {
    interface  View: BaseView {
        fun onRegisterSuccess(loginResponse: LoginResponse,view:android.view.View)
        fun onRegisterPhotoSuccess(view:android.view.View)
        fun onRegisterFailed(message:String)

    }
    interface  Presenter : SignupContract, BasePresenter {
        fun submitRegister(registerRequest: RegisterRequest, view:android.view.View)
        fun submitPhotoRegister(filePath: Uri, view:android.view.View)

    }
}