package com.example.damsobiapp.ui.profile.account

import android.util.Log
import com.example.damsobiapp.network.HttpClient
import com.example.damsobiapp.ui.auth.signin.SigninContract
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class  SignoutPresenter (private  val view: SignoutContract.View) :SignoutContract.Presenter {

   private val mCompositeDisposable : CompositeDisposable?
   init{
       this.mCompositeDisposable = CompositeDisposable()
   }

    override fun submitLogout() {
        view.showLoading()
        val disposable = HttpClient.getInstance().getApi()!!.logout()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                {
                    view.dismissLoading()
                    if (it.meta?.status.equals("success", true)) {
                             view.onLogoutSuccess()
                    } else {
                        it.meta?.message?.let { it1 -> view.onLogoutFailed(it1) }
                    }

                 },
                {
                    view.dismissLoading()
                    view.onLogoutFailed(it.message.toString())
                }
            )


        mCompositeDisposable!!.add(disposable)
    }

    override fun subscribe() {

    }

    override fun unsubscribe() {
        mCompositeDisposable!!.clear()
    }
}