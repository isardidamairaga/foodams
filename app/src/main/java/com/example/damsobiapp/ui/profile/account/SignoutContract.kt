package com.example.damsobiapp.ui.profile.account

import com.example.damsobiapp.base.BasePresenter
import com.example.damsobiapp.base.BaseView

interface SignoutContract {
    interface  View: BaseView {
        fun onLogoutSuccess()
        fun onLogoutFailed(message:String)

    }
    interface  Presenter : SignoutContract, BasePresenter {
        fun submitLogout()

    }
}