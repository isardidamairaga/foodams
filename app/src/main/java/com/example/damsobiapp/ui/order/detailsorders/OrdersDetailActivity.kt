package com.example.damsobiapp.ui.order.detailsorders

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.Navigation
import androidx.navigation.fragment.NavHostFragment
import com.example.damsobiapp.R
import com.example.damsobiapp.ui.detail.DetailFragment
import kotlinx.android.synthetic.main.layout_toolbar.*


class OrdersDetailActivity : AppCompatActivity() {
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_orders)
        intent.extras?.let {
//            val navHostFragment = supportFragmentManager.findFragmentById(R.id.detailOrdersHostFragment) as NavHostFragment
//            val navController = navHostFragment.navController
            val bundle = Bundle()
            var fragment =  OrdersDetailFragment()
            bundle.putParcelable("data", it.get("data") as Parcelable?)
            fragment.arguments = bundle
            getSupportFragmentManager().beginTransaction()
                .replace(R.id.flAda, fragment)
                .commit()
//            navController.navigate(R.id.nav_detail,bundle)
//            navController.setGraph(R.navigation.nav_detail_orders, bundle)
        }
    }

    fun toolbarPayment() {
        toolbar.visibility = View.VISIBLE
        toolbar.title = "Payment"
        toolbar.subtitle = "You deserve better meal"
        toolbar.navigationIcon = resources.getDrawable(R.drawable.ic_arrow_back_000)
        toolbar.setNavigationOnClickListener { onBackPressed() }
    }
}