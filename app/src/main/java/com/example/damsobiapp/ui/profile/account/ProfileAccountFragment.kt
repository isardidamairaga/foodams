package com.example.damsobiapp.ui.profile.account

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.damsobiapp.FOODAMS
import com.example.damsobiapp.MainActivity
import com.example.damsobiapp.R
import com.example.damsobiapp.model.dummy.ProfileMenuModel
import com.example.damsobiapp.ui.auth.AuthActivity
import com.example.damsobiapp.ui.order.OrderPresenter
import com.example.damsobiapp.ui.profile.ProfileMenuAdapter
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_profile_account.*


class ProfileAccountFragment : Fragment(), ProfileMenuAdapter.ItemAdapterCallback,SignoutContract.View{

    private var  menuArrayList: ArrayList<ProfileMenuModel> = ArrayList()
    lateinit var presenter: SignoutPresenter
    var progressDialog : Dialog?=null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile_account, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = SignoutPresenter(this)
        initDataDummy()
        var adapter = ProfileMenuAdapter(menuArrayList,this)
        var layoutManager : RecyclerView.LayoutManager = LinearLayoutManager(activity)
        rcList.layoutManager=layoutManager
        rcList.adapter = adapter
    }
    fun initDataDummy (){
        menuArrayList= ArrayList()
        menuArrayList.add(ProfileMenuModel("Edit Profile"))
        menuArrayList.add(ProfileMenuModel("Home Address"))
        menuArrayList.add(ProfileMenuModel("Security"))
        menuArrayList.add(ProfileMenuModel("Payments"))
        menuArrayList.add(ProfileMenuModel("Logout"))

    }

    override fun onClick(v: View, data: ProfileMenuModel) {

        if (data.tittle === "Logout"){
            presenter.submitLogout()

        }else {
            Toast.makeText(context, "ini menu yang kamu klik " + data.tittle, Toast.LENGTH_SHORT)
                .show()
        }
    }

    override fun onLogoutSuccess() {
        val back = Intent(activity, AuthActivity::class.java)
        FOODAMS.getApp().unsetToken()
        FOODAMS.getApp().unsetUser()
        startActivity(back)
        activity?.finish()
    }

    override fun onLogoutFailed(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun showLoading() {
        progressDialog?.show()
    }

    override fun dismissLoading() {
        progressDialog?.dismiss()
    }

}