package com.example.damsobiapp.ui.home.newtaste

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.damsobiapp.R
import com.example.damsobiapp.model.dummy.HomeModel
import com.example.damsobiapp.model.dummy.HomeVerticalModel
import com.example.damsobiapp.model.response.home.Data
import com.example.damsobiapp.ui.detail.DetailActivity
import com.example.damsobiapp.ui.home.HomeAdapter
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.rcList
import kotlinx.android.synthetic.main.fragment_home_new_taste.*

class HomeNewTasteFragment : Fragment(), HomeNewtasteAdapter.ItemAdapterCallback {
//    private var foodlist :  ArrayList<HomeVerticalModel> = ArrayList()
    private var newTasteList : ArrayList<Data>? =ArrayList()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_new_taste, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        newTasteList = arguments?.getParcelableArrayList("data")

//        initDataDummy()
        var adapter = HomeNewtasteAdapter(newTasteList!!, this)
        var layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity)
        rcList.layoutManager =layoutManager
        rcList.adapter = adapter
    }

//    fun initDataDummy(){
//        foodlist = ArrayList()
//        foodlist.add(HomeVerticalModel("Cherry Healthy","10000","",5f))
//        foodlist.add(HomeVerticalModel("Burger Mayo","30000","",4f))
//        foodlist.add(HomeVerticalModel("Bakwan cihuy","2000","",4.5f))
//
//    }

    override fun onClick(v: View?, data: Data) {
        val detail = Intent (activity,DetailActivity::class.java).putExtra("data",data)
        startActivity(detail)
    }

}