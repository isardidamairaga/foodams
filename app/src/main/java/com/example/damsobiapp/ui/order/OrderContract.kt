package com.example.damsobiapp.ui.order

import com.example.damsobiapp.base.BasePresenter
import com.example.damsobiapp.base.BaseView
import com.example.damsobiapp.model.response.home.HomeResponse
import com.example.damsobiapp.model.response.transaction.TransactionResponse

interface OrderContract {
    interface  View: BaseView {
        fun onTransactionSuccess(transactionResponse: TransactionResponse)
        fun onTransactionFailed(message:String)

    }
    interface  Presenter : OrderContract, BasePresenter {
        fun getTransaction()

    }
}