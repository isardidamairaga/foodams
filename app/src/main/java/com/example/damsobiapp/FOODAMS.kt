package com.example.damsobiapp

import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.multidex.MultiDexApplication
import com.example.damsobiapp.network.HttpClient
import kotlinx.coroutines.selects.SelectInstance

class FOODAMS : MultiDexApplication(){
    companion object{
        lateinit var  instance: FOODAMS

        fun getApp() : FOODAMS{
            return instance
        }
    }

    override fun onCreate() {
        super.onCreate()
        instance=this
    }

    fun getPreferences():SharedPreferences{
        return PreferenceManager.getDefaultSharedPreferences(this)
    }
    fun setToken(token:String){
        getPreferences().edit().putString("PREFERENCES_TOKEN",token).apply()
        HttpClient.getInstance().buildRetrofitClient(token)
    }
    fun getToken():String?{
        return getPreferences().getString("PREFERENCES_TOKEN",null)
    }
    fun setUser(user:String){
        getPreferences().edit().putString("PREFERENCES_USER",user).apply()
//        HttpClient.getInstance().buildRetrofitClient(user)
    }
    fun getUser():String?{
        return getPreferences().getString("PREFERENCES_USER",null)
    }
    fun unsetUser(){
        getPreferences().edit().remove("PREFERENCES_USER")
    }
    fun unsetToken(){
        getPreferences().edit().remove("PREFERENCES_TOKEN")
    }

}