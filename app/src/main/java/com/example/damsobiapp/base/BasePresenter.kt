package com.example.damsobiapp.base

interface BasePresenter {
    fun subscribe()
    fun unsubscribe()
}