package com.example.damsobiapp.base

interface BaseView {
    fun showLoading()
    fun dismissLoading()
}